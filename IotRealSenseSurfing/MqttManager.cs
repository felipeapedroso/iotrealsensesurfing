﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace IotRealSenseSurfing
{
    class MqttManager
    {
        #region Singleton Implementation
        private static MqttManager instance;

        public static MqttManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MqttManager();
                }

                return instance;
            }
        }

        #endregion

        private MqttClient mqttClient;
        public string ClientId { get; private set; }
        
        private DateTime LastTimeMessage;
        public double DelayBetweenMessages { get; set; }

        private MqttManager() 
        {
            DelayBetweenMessages = 0;
        }

        public void Connect(string serverHostName)
        {
            if (mqttClient == null)
            {
                mqttClient = new MqttClient(serverHostName);
                ClientId = Guid.NewGuid().ToString();
                mqttClient.Connect(ClientId);
            }
        }

        public void Disconnect()
        {
            if (mqttClient != null)
            {
                mqttClient.Disconnect();
                mqttClient = null;
            }
        }

        public void SendMqttMessage(String topic, String message)
        {
            if (string.IsNullOrEmpty(topic.Trim()) || string.IsNullOrEmpty(message.Trim()))
            {
                return;
            }

            Console.WriteLine("Topic: {0}, Message:{1}", topic, message);

            if (mqttClient != null && mqttClient.IsConnected)
            {
                DateTime now = DateTime.Now;

                if (LastTimeMessage == null || now.Subtract(LastTimeMessage).TotalMilliseconds >= DelayBetweenMessages)
                {
                    mqttClient.Publish(topic, Encoding.UTF8.GetBytes(message), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
                    LastTimeMessage = now;
                }
            }
            else
            {
                Console.WriteLine("The MqttClient is not connected.");
            }
        }
    }
}
