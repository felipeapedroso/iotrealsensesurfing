﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotRealSenseSurfing
{
    class RealSenseManager
    {
        #region Singleton Implementation
        private static RealSenseManager instance;
        
        public static RealSenseManager Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new RealSenseManager();
                }

                return instance;
            }
        }

        #endregion

        private PXCMSenseManager senseManager;
        private PXCMHandData handData;

        private const string GESTURE_CLICK = "click";
        private const string GESTURE_VSIGN = "v_sign";
        private const string GESTURE_FIST = "fist";
        private const string GESTURE_SPREADFINGERS = "spreadfingers";
       
        private String lastGestureDetected;
        
        public String[] enabledGestures = { GESTURE_CLICK, GESTURE_VSIGN, GESTURE_FIST, GESTURE_SPREADFINGERS };

        public event GestureDetectedEventHandler GestureDetected;
        private long lastGestureTimeStamp;
        private PXCMHandData.BodySideType lastGestureBodySide;

        private RealSenseManager() 
        {
 
        }

        private void InitializeFaceModule()
        {
            if (senseManager == null)
            {
                return;
            }

            // TODO
        }

        private void InitializeHandModule()
        {
            if (senseManager == null)
            {
                return;    
            }

            if (senseManager.EnableHand().IsError())
            {
                senseManager = null;
                Console.WriteLine("Failed to enable the hand module.");
                return;
            }

            PXCMHandModule handModule = senseManager.QueryHand();

            PXCMHandConfiguration handConfiguration = handModule.CreateActiveConfiguration();
            //handConfiguration.EnableAllGestures();

            foreach (var gesture in enabledGestures)
            {
                handConfiguration.EnableGesture(gesture);
            }

            handConfiguration.ApplyChanges();

            handData = handModule.CreateOutput();

            lastGestureDetected = "";
        }

        public void Start()
        {
            if (senseManager == null)
            {
                senseManager = PXCMSenseManager.CreateInstance();

                InitializeHandModule();
                InitializeFaceModule();

                PXCMSenseManager.Handler handler = new PXCMSenseManager.Handler();
                handler.onModuleProcessedFrame = OnModuleProcessedFrame;

                if (senseManager != null)
                {
                    senseManager.Init(handler);
                    senseManager.StreamFrames(false);                    
                }
            }
        }

        public void Stop()
        {
            if (senseManager != null)
            {
                senseManager.Close();
                senseManager.Dispose();
                senseManager = null;
            }
        }

        private pxcmStatus OnModuleProcessedFrame(int mid, PXCMBase module, PXCMCapture.Sample sample)
        {
            switch (mid)
            {
                case PXCMHandModule.CUID:
                    return ProcessHandData();
                case PXCMFaceModule.CUID:
                    return ProcessFaceData();
                default:
                    break;
            }

            return pxcmStatus.PXCM_STATUS_NO_ERROR;
        }

        private pxcmStatus ProcessFaceData()
        {
            return pxcmStatus.PXCM_STATUS_NO_ERROR;
        }

        private pxcmStatus ProcessHandData()
        {
            pxcmStatus status;
            status = handData.Update();

            if (status.IsError())
            {
                return status;
            }

            ProcessGestures();

            return pxcmStatus.PXCM_STATUS_NO_ERROR;
        }

        private void ProcessGestures()
        {
            int firedGesturesNumber = handData.QueryFiredGesturesNumber();

            for (int i = 0; i < firedGesturesNumber; i++)
            {
                PXCMHandData.GestureData gestureData;

                if (handData.QueryFiredGestureData(i, out gestureData).IsSuccessful())
                {
                    PXCMHandData.GestureStateType gestureState = gestureData.state;

                    if (gestureState == PXCMHandData.GestureStateType.GESTURE_STATE_START)
                    {
                        String gestureName = gestureData.name;
                        long timestamp = gestureData.timeStamp;

                        PXCMHandData.IHand handInfo;
                        if (handData.QueryHandDataById(gestureData.handId, out handInfo).IsError())
                        {
                            continue;
                        }

                        PXCMHandData.BodySideType bodySideHand = handInfo.QueryBodySide();

                        bool isSameGesture = gestureName.CompareTo(lastGestureDetected) == 0;
                        bool isSameTimeStamp = timestamp == lastGestureTimeStamp;
                        bool isSameBodySide = bodySideHand == lastGestureBodySide;

                        if (!isSameGesture || (isSameGesture && (!isSameTimeStamp || !isSameBodySide)))
                        {
                            lastGestureDetected = gestureName;
                            lastGestureTimeStamp = timestamp;
                            lastGestureBodySide = bodySideHand;

                            if (GestureDetected != null)
                            {
                                GestureDetected(this, new GestureArgs(gestureName, bodySideHand, timestamp));
                            }
                        }
                    }
                }
            }
        }

        public delegate void GestureDetectedEventHandler(object sender, GestureArgs args);

        public class GestureArgs : EventArgs
        {
            public String GestureName { get; private set; }
            public PXCMHandData.BodySideType BodySide { get; private set; }
            public long TimeStamp { get; private set; }

            public GestureArgs(String gestureName, PXCMHandData.BodySideType bodySideHand, long timeStamp)
            {
                GestureName = gestureName;
                BodySide = bodySideHand;
                TimeStamp = timeStamp;
            }

            public override string ToString()
            {
                return string.Format("GestureName: {0}, BodySide: {1}, TimeStamp:{2}", GestureName, BodySide, TimeStamp);
            }
            
        }
    }
}
