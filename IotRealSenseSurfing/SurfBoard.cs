﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotRealSenseSurfing
{
    class SurfBoardHelper
    {
        public const String DefaultSurfBoard = "surfboard";
        public const String MqttTopic = "globalcode/things";
        public const String MqttServerHostname = "iot.eclipse.org";

        public static void Connect() 
        {
            MqttManager.Instance.Connect(MqttServerHostname);
        }

        public static void Disconnect()
        {
            MqttManager.Instance.Disconnect();
        }

        public static void SetRed(String target, int value)
        {
            if (value > 255)
            {
                value = 255;
            }
            else if (value < 0)
            {
                value = 0;
            }

            SendCommand(target, "red", value);
        }

        public static void SetGreen(String target, int value)
        {
            if (value > 255)
            {
                value = 255;
            }
            else if (value < 0)
            {
                value = 0;
            }

            SendCommand(target, "green", value);
        }

        public static void SetBlue(String target, int value)
        {
            if (value > 255)
            {
                value = 255;
            }
            else if (value < 0)
            {
                value = 0;
            }

            SendCommand(target, "blue", value);
        }

        public static void SetColor(String target, int r, int g, int b)
        {
            SetRed(target, r);
            SetGreen(target, g);
            SetBlue(target, b);
        }

        public static void SetRelay(String target, bool status)
        {
            int value = status ? 1 : 0;

            SendCommand(target, "relay", value);
        }

        public static void SetSpeaker(String target, bool status)
        {
            int value = status ? 1 : 0;

            SendCommand(target, "speaker", value);
        }

        public static void SetLcdText(String target, string value)
        {
            SetLcdText(target, 0, value);
        }

        public static void SetLcdText(String target, int lcdId, string value) 
        {
            StringBuilder lcdCmd = new StringBuilder();

            lcdCmd.Append("lcd");

            if (lcdId > 0)
            {
                lcdCmd.Append(lcdId);
            }

            SendCommand(target, lcdCmd.ToString(), value);
        }

        public static void SendCommand(string target, string command, string value)
        {
            MqttManager.Instance.SendMqttMessage(MqttTopic, CreateSurfboardCommand(target, command, value));
        }

        public static void SendCommand(string target, string command)
        {
            SendCommand(target, command, string.Empty);
        }

        public static void SendCommand(string target, string command, int value)
        {
            SendCommand(target, command, value.ToString());
        }

        public static void SendCommand(string target, string command, bool value)
        {
            SendCommand(target, command, value.ToString());
        }

        public static void SendCommand(string target, string command, float value)
        {
            SendCommand(target, command, value.ToString());
        }

        public static void SendCommand(string target, string command, double value)
        {
            SendCommand(target, command, value.ToString());
        }

        public static void SendCommand(string target, string command, object value)
        {
            SendCommand(target, command, value.ToString());
        }

        private static string CreateSurfboardCommand(string target, string command, string value)
        {
            StringBuilder commandBuilder = new StringBuilder();

            commandBuilder.Append(!string.IsNullOrEmpty(target.Trim()) ? target : DefaultSurfBoard);

            if (!string.IsNullOrEmpty(command.Trim()))
            {
                commandBuilder.AppendFormat("/{0}", command);
            }

            if (!string.IsNullOrEmpty(value.Trim()))
            {
                commandBuilder.AppendFormat("?{0}", value);
            }

            return commandBuilder.ToString();
        }
    }
}
