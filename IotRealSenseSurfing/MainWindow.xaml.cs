﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IotRealSenseSurfing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SurfBoardHelper.Connect();

            RealSenseManager.Instance.Start();
            RealSenseManager.Instance.GestureDetected += GestureDetected;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SurfBoardHelper.Disconnect();
            RealSenseManager.Instance.Stop();
        }

        private void GestureDetected(object sender, RealSenseManager.GestureArgs args)
        {
            Console.WriteLine(args);
            SurfBoardHelper.SetRelay("surfboard1", true);
        }


    }
}
