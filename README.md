# README #

### What is this repository for? ###

This is a simple demo to show how to integrate Mqtt with RealSense. The idea is to control the IOT Sufboard using gestures and facial expressions.

### How do I get set up? ###

This project was developed using Visual Studio 2013 Community and is using the Intel(R) RealSense(TM) SDK 6.0.21.6598.

If the MQTT libraries doesn't work, please try to install the M2Mqtt library on Visual Studio using the following steps (or the ones available on https://www.nuget.org/packages/M2Mqtt/):

* Open "Package Manager Console" (Tools -> NuGet Package Manager -> Package Manager Console)
* Type "Install-Package M2Mqtt"
* Profit!

### Who do I talk to? ###

* Felipe Pedroso <felipe.pedroso@intel.com>